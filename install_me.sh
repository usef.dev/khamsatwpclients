#!/bin/sh
apt-get update
apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update
apt install docker-ce
docker container run --name spawning-pool -d --restart on-failure -e MYSQL_ROOT_PASSWORD=khamsat -v hatchery -e MYSQL_DATABASE=khamsat  mysql:5.7
docker run --name lair --link spawning-pool:mysql -p 80:80 -d wordpress